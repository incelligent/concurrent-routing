#!/usr/bin/python

import json
import sys
import netifaces
import subprocess
import math
import datetime
import csv

CONFIG = {
	"zmqport": "tcp://172.17.0.1:5556"
	}

# Configuration File
CONFIGFILE = '/opt/vmixos/config'

# Default Values
EXPCONFIG = {
        "w1": 1,
        "w2": 1,
        "w3": 1,
        "w4": 1,
        "w5": 1,
        "w6": 1,
        "w7": 0.01,
        "c1": 1000000,
        "c2": 1,
        "c3": 1,
        "c4": 0.01,
        "c5": 0.01,
        "c6": 1,
        "c7": 1,
        "plossmax": 100,
        "jittermax": 300
        }

def main():
    # try:
    #     with open(CONFIGFILE) as configfd:
    #         EXPCONFIG.update(json.load(configfd))
    # except Exception as e:
    #     print "Cannot retrive expconfig {}".format(e)
    CRITICAL_IP = "193.92.70.136"
    CRITICAL_PORT = "8844"
    NON_CRITICAL_IP = "91.138.176.151"
    NON_CRITICAL_PORT = "5001"
    # Configure ip tables
    # Generating traffic for critical ip
    output = subprocess.check_output(["iperf3", "-c", NON_CRITICAL_IP, "-p", NON_CRITICAL_PORT, "-t", "2"])

    print output

if __name__ == "__main__":
    main()
